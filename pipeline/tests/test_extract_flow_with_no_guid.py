import pytest
from pipeline.validate_dfi import extract_flows_with_no_guid, FlowValidationError
from unittest import TestCase


class TestExtractFlowsWithNoGuid(TestCase):

    def test_extract_one_flow_without_guid(self):
        guid1 = ["AT00001", "AT00002", "Alert", "Alerts", "SFTP", "TDS", "", "Alerts description", "SDLC-12345", "", "",
                 "CORCID00001"]
        guid0 = ["AT00001", "AT00002", "Alert", "Alerts", "SFTP", "TDS", "", "Alerts description", "SDLC-12345", "", "",
                 ""]
        guidNone = ["AT00001", "AT00002", "Alert", "Alerts", "SFTP", "TDS", "", "Alerts description", "SDLC-12345", "",
                    "", None]
        dfi = [guid1, guid0, guidNone]
        result = extract_flows_with_no_guid(dfi)
        self.assertEqual(len(dfi), 3)
        self.assertEqual(len(result), 2)

    def test_extract_zero_flows_without_guid(self):
        guid1 = ["AT00001", "AT00002", "Alert", "Alerts", "SFTP", "TDS", "", "Alerts description", "SDLC-12345", "", "",
                 "CORCID00001"]
        dfi_3_guid = [guid1, guid1, guid1]
        result = extract_flows_with_no_guid(dfi_3_guid)
        self.assertEqual(len(dfi_3_guid), 3)
        self.assertEqual(len(result), 0)

    def test_invalid_dfi_format_exception(self):
        guid_error = ["AT00001", "AT00002", "Alert", "Alerts", "SFTP", "TDS", "", "Alerts description", "SDLC-12345",
                      "", ""]  # this list doesnt have 12 items
        with pytest.raises(FlowValidationError):
            extract_flows_with_no_guid(guid_error)
