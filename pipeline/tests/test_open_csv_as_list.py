from pipeline.validate_dfi import open_csv_as_list
from unittest import TestCase, mock


class TestFileHandler(TestCase):

    def test_import_empty_file(self):
        result = open_csv_as_list("pipeline/tests/data/empty_file.csv")
        self.assertFalse(result)

    def test_import_only_header_file(self):
        result = open_csv_as_list("pipeline/tests/data/only_header_file.csv")
        self.assertFalse(result)

    def test_import_valid_file_with_3_lines(self):
        result = open_csv_as_list("pipeline/tests/data/header_and_3_lines.csv")
        self.assertTrue(result)
        self.assertEqual(len(result), 3)

#    def test_file_with_empty_line_in_the_middle(self):
#        self.assertTrue(False)

 #   def test_file_with_more_than_1_empty_trailling_line(self):
 #       self.assertTrue(False)