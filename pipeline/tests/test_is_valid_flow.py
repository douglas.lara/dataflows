import pytest
from unittest import TestCase, mock
from pipeline.validate_dfi import is_valid_flow, FlowValidationError


class TestIsValidFlow(TestCase):

    def test_flow_is_correct_and_has_no_guid(self):
        flow = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        with pytest.raises(FlowValidationError):
            is_valid_flow(flow)

    def test_flow_is_correct_and_has_no_guid_2(self):
        flow = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        self.assertFalse(is_valid_flow(flow))

    def test_flow_is_correct_and_has_no_guid_3(self):
        flow = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, None]
        self.assertTrue(is_valid_flow(flow))

    def test_flow_is_correct_and_has_no_guid_4(self):
        flow = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ""]
        self.assertTrue(is_valid_flow(flow))

    def test_flow_is_correct_and_has_no_guid_5(self):
        flow = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
        with pytest.raises(FlowValidationError):
            is_valid_flow(flow)