from unittest import TestCase, mock
from pipeline import validate_dfi


class TestValidateDfi(TestCase):

    @mock.patch('pipeline.validate_dfi.open_csv_as_list', return_value=[])
    def test_new_flows_validation(self, mock_open_csv):
        validate_dfi.new_flows_validation('dfi.csv')
        mock_open_csv.assert_called_with("dfi.csv")

    @mock.patch('pipeline.validate_dfi.extract_flows_with_no_guid', side_effect=IndexError())
    def test_parsing_error_exit_failure(self, mock_extract_flows_with_no_guid):
        exit_code = validate_dfi.new_flows_validation('dfi.csv')
        mock_extract_flows_with_no_guid.assert_called()
        self.assertEqual(exit_code, 1)

    # @mock.patch('pipeline.validate_dfi.main', return_value=0)
    # @mock.patch('sys.exit')
    # def test_default_main(self, mock_main, mock_sys):
    #     pipeline.validate_dfi
    #     mock_main.assert_called
    #     mock_sys.assert_called_with(0)

    def test_init(self):
        from pipeline import validate_dfi
        with mock.patch.object(validate_dfi, "new_flows_validation", return_value=0):
            with mock.patch.object(validate_dfi, "__name__", "__main__"):
                with mock.patch.object(validate_dfi.sys, 'exit') as mock_exit:
                    with mock.patch.object(validate_dfi.sys, 'argv') as mock_argv:
                        validate_dfi.init()
                        mock_exit.assert_called_with(0)
