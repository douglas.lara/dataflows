"""Module docstring"""
import csv
import os
import sys
from enum import IntEnum

ERROR_EXIT_CODE = 1
SUCCESS_EXIT_CODE = 0

class DfiIndex(IntEnum):
    SOURCE_SWCI = 0
    TARGET_SWCI = 1
    GUID = 11


# define Python user-defined exceptions
class FlowValidationError(Exception):
    """Base class for other exceptions"""
    pass

def extract_flows_with_no_guid(dfi_list):
    """For every list item, try to read the in the array the position expecte to have the GUID"""
    return [flow for flow in dfi_list if is_valid_flow(flow)]

def is_valid_flow(flow):
    if flow != None and len(flow) == 12:
        return (flow[DfiIndex.GUID] == "" or flow[DfiIndex.GUID] == None)
    else:
        if(len(flow) != 12):

            raise FlowValidationError("Flow " + str(flow) + " should have 12 columns" )
        else:
            return False


def new_flows_validation(dfi_filepath):
    """Conduct the validation procedure of the changes submitted, if any.
    Returns success if items missing GUID are not found"""

    # best option would be to open the dfi in backwards mode since new flows are appendend last.
    # there is a lib but need to check if it is available.
    dfi = open_csv_as_list(dfi_filepath)
    try:
        new_flows = extract_flows_with_no_guid(dfi)
    except IndexError:
        print("DFI file: Inconsistent format")
        return ERROR_EXIT_CODE

    if not new_flows:
        return SUCCESS_EXIT_CODE

    print("Found " + str(len(new_flows)) + " new flows")
    validate_source_and_target(new_flows)

    # isac = open_csv_as_list('isac.csv')
    # gdd = open_csv_as_list('gdd.csv')

    return SUCCESS_EXIT_CODE


def open_csv_as_list(filename):
    """Reads a CSV file and return the content as a list of lists, excluding the file header"""
    if os.stat(filename).st_size == 0:
        return []

    with open(filename, mode='r') as infile:
        next(infile)  # skip header
        reader = csv.reader(infile)
        csv_as_list = list(reader)

    return csv_as_list


def validate_source_and_target(new_flows):
    """"Validate if all source and targets are ISAC instance numbers and if those instances are valid"""
    isac = open_csv_as_list("isac.csv")
    for feed in new_flows:
        source = feed[DfiIndex.SOURCE_SWCI]
        target = feed[DfiIndex.TARGET_SWCI]
        # find the isac item for the source/target
        # validate it is not in DEC3/HIST
        # warning if in DEC1, DEC2, DEV

def init():
    """Conduct all validations"""
    if __name__ == "__main__":
        dfi_filepath = sys.argv[1] if len(sys.argv) > 1 else "dfi.csv"
        exit_code = new_flows_validation(dfi_filepath)
        sys.exit(exit_code)


init()
