# DataFlows

DataFlows is designed to have the following main components:

```plantuml
skinparam componentStyle uml2

(*)--> "New Amendmment MR"
"New Amendmment MR" --> "Test Validation Scripts"
"Test Validation Scripts" --> ==B1==
==B1== --> "Validate Source App"
==B1== --> "Validate Target App"
==B1== --> "Validate DCP Instance"
"Validate Source App" --> "Validate Domain Entity MAS Status"
"Validate Domain Entity MAS Status" --> ==B2==
"Validate Target App" --> ==B2==
"Validate DCP Instance" --> ==B2==
==B2== --> "<i><manual task></i>\nAttribute IDs\n*manually*\nin the branch" as ID1
ID1 --> "Check for Duplicate IDs" 
"Check for Duplicate IDs" --> ==B3==
==B3== --> "Render Data Flow Diagram"
==B3== --> "Calculate DFI Metrics"
"Render Data Flow Diagram" --> ==B4==
"Calculate DFI Metrics" --> ==B4==
==B4== --> "Merge with MASTER"
"Merge with MASTER" --> (*)
```


